# frozen_string_literal: true

require_relative "boot"

run Application::AutoloadHandler.new(App)
