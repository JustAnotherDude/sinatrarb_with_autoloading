# frozen_string_literal: true

module Application
  extend self
  include Concurrent::Synchronization::Volatile

  class AutoloadHandler
    def initialize(app_class)
      @app_class = app_class
      @class_name = @app_class.name
    end

    def call(env)
      return @app_class.call(env) unless Application.env.development?

      if Application.files_changed
        Application.rw_lock.with_write_lock do
          next unless Application.files_changed

          Application.loader.reload
          @app_class = @class_name.constantize
          Application.files_changed = false
        end
      end

      # Write-lock waits for all acquired read-locks.
      # Also, new acquired read-locks will block thread, until write-lock is released.
      Application.rw_lock.with_read_lock { @app_class.call(env) }
    end
  end

  # We want to use active support inflector instead of standard one.
  class Inflector
    def camelize(basename, _absolute_path)
      basename.camelize
    end
  end

  module ConsoleMethods
    # Rails style: reload constants in console.
    def reload!
      Application.loader.reload
    end
  end

  DEFAULT_RACK_ENV = "development"
  DIRS_TO_LOAD = %w[applications services].freeze

  attr_accessor :loader
  attr_accessor :listener
  attr_volatile :files_changed

  def setup!
    ENV["RACK_ENV"] ||= DEFAULT_RACK_ENV
    setup_loader!
    setup_listener! if env.development?
  end

  def extend_with_console_methods!(instance)
    instance.extend(ConsoleMethods)
  end

  def root
    @root ||= Pathname(__dir__)
  end

  # Rails way again.
  def env
    @env ||= ActiveSupport::StringInquirer.new(ENV["RACK_ENV"])
  end

  # We need to define one read-write lock globally in main thread.
  def rw_lock
    @rw_lock ||= Concurrent::ReadWriteLock.new
  end

  private

  def setup_loader!
    self.loader = initialize_loader
  end

  # Use eager load in environment, any other than development.
  def initialize_loader
    Zeitwerk::Loader.new.tap do |loader|
      dirs_to_load.map(&loader.method(:push_dir))
      loader.inflector = Inflector.new
      loader.enable_reloading if env.development?
      loader.setup
      loader.eager_load unless env.development?
    end
  end

  # Root dirs to use with loader.
  def dirs_to_load
    @dirs_to_load ||= DIRS_TO_LOAD.map { |relative_path| root.join(relative_path) }
  end

  def setup_listener!
    self.listener = initialize_listener
    self.files_changed = false

    listener.start
  end

  # Define listener, which will change flag of files state, when files are changed.
  def initialize_listener
    Listen.to(*dirs_to_load) { self.files_changed = true }
  end
end
