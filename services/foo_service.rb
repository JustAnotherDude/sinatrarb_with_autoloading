# frozen_string_literal: true

class FooService < ApplicationService
  def call
    success!(status: "success", data: data)
  end

  private

  def data
    { message: "Try to change me" }
  end
end
