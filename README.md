# README

Just simple Sinatra application with integrated autoloading by [Zeitwerk](https://github.com/fxn/zeitwerk).

## Base Information

1. Here we have the simple web app, based on [Sinatra](http://sinatrarb.com/intro.html).
2. I use autoload library: [Zeitwerk](https://github.com/fxn/zeitwerk).
3. Make "Thin" run application in threaded environment: each request - new thread.
4. Write thread-safe autoload by: defining middleware and using
[read-write locks](https://ruby-concurrency.github.io/concurrent-ruby/1.1.5/Concurrent/ReadWriteLock.html).

## How to run

```bash
$ thin start -C thin.yml
```
