# frozen_string_literal: true

class App < Sinatra::Base
  disable :static
  disable :sessions

  set :logging, true
  set :root, Application.root

  get "/" do
    json(FooService.call)
  end

  get "/about" do
    "Awesome Zeitwerk"
  end
end
