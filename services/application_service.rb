# frozen_string_literal: true

class ApplicationService
  attr_accessor :response

  def self.call(*args)
    new(*args).tap(&:call).response
  end

  private

  def success!(data)
    self.response = data
  end
end
